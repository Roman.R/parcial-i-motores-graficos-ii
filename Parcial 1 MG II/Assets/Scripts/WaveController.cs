﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveController : MonoBehaviour
{
    public GameObject zombiePrefab;  // Prefab del zombi
    public float spawnInterval = 2f;  // Intervalo entre generaci�n de zombis
    public int zombiesPerWave = 10;  // Cantidad de zombis por oleada
    public float waveInterval = 5f;  // Intervalo entre oleadas
    private int currentWave = 1;  // Oleada actual
    private int zombiesSpawned = 0;  // Contador de zombis generados
    private bool isSpawning = false;  // Flag para controlar la generaci�n de zombis

    public global::System.Boolean IsSpawning { get => isSpawning; set => isSpawning = value; }

    void Start()
    {
        StartWave();  // Inicia la primera oleada
    }

    void StartWave()
    {
        IsSpawning = true;
        zombiesSpawned = 0;
        InvokeRepeating("SpawnZombie", spawnInterval, spawnInterval);
    }

    void SpawnZombie()
    {
        global::System.Object value = Instantiate(zombiePrefab, transform.position, Quaternion.identity);
        zombiesSpawned++;

        if (zombiesSpawned >= zombiesPerWave)
        {
            IsSpawning = false;
            currentWave++;
            CancelInvoke("SpawnZombie");
            Invoke("StartNextWave", waveInterval);
        }
    }

    void StartNextWave()
    {
        StartWave();
    }
}
