using UnityEngine;

public class MusicController : MonoBehaviour
{
    
    public AudioClip gameMusic;

    private AudioSource audioSource;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = gameMusic;
        audioSource.Play();
    }
}
