
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlMenuPrincipal : MonoBehaviour
{
    public void Jugar()
    {
        SceneManager.LoadScene("JuegoPrincipal");
    }

    public void Salir()
    {
        Debug.Log("Ya sali del juego");
        Application.Quit();
    }
}
