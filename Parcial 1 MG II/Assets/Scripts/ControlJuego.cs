using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlJuego : MonoBehaviour
{
    public GameObject jugador;
    public GameObject zombi;
    private List<GameObject> listaEnemigos = new List<GameObject>();
    public TMPro.TMP_Text gameover;

    void Start()
    {
        gameover.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            recibirDaņo();
        }
    }

    public void recibirDaņo()
    {
        gameover.text = "GAME OVER";
        
    }
}
